﻿namespace StudioRL2._0
{
    partial class Consulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtApelido = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnConcluir = new System.Windows.Forms.Button();
            this.cboFreq = new System.Windows.Forms.ComboBox();
            this.chkPagar = new System.Windows.Forms.CheckBox();
            this.chkData = new System.Windows.Forms.CheckBox();
            this.chkFrequ = new System.Windows.Forms.CheckBox();
            this.chkCorte = new System.Windows.Forms.CheckBox();
            this.chkLapis = new System.Windows.Forms.CheckBox();
            this.chkBarba = new System.Windows.Forms.CheckBox();
            this.chkPezinho = new System.Windows.Forms.CheckBox();
            this.chkLuzes = new System.Windows.Forms.CheckBox();
            this.chkPigmentacaoBarba = new System.Windows.Forms.CheckBox();
            this.chkSombrancelha = new System.Windows.Forms.CheckBox();
            this.chkPigmentacaoCorte = new System.Windows.Forms.CheckBox();
            this.chkSombrancelhaHenna = new System.Windows.Forms.CheckBox();
            this.chkGel = new System.Windows.Forms.CheckBox();
            this.chkRelaxamento = new System.Windows.Forms.CheckBox();
            this.chkProgressiva = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.chkInfantil = new System.Windows.Forms.CheckBox();
            this.lblInfantil = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cboOperadora = new System.Windows.Forms.ComboBox();
            this.txtNomeCompleto = new System.Windows.Forms.TextBox();
            this.chkMensalista = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mskTelFixo = new System.Windows.Forms.MaskedTextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.mskTelCel = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnEditar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lblwhats = new System.Windows.Forms.Label();
            this.lblMensalista = new System.Windows.Forms.Label();
            this.chkWhats = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.ForeColor = System.Drawing.Color.Black;
            this.txtNome.Location = new System.Drawing.Point(244, 29);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(422, 31);
            this.txtNome.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(165, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nome";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Location = new System.Drawing.Point(683, 20);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(102, 40);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtApelido
            // 
            this.txtApelido.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApelido.ForeColor = System.Drawing.Color.Black;
            this.txtApelido.Location = new System.Drawing.Point(244, 66);
            this.txtApelido.Name = "txtApelido";
            this.txtApelido.Size = new System.Drawing.Size(422, 31);
            this.txtApelido.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(149, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Apelido";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(12, 133);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1182, 392);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtValor);
            this.tabPage1.Controls.Add(this.dateTimePicker1);
            this.tabPage1.Controls.Add(this.btnConcluir);
            this.tabPage1.Controls.Add(this.cboFreq);
            this.tabPage1.Controls.Add(this.chkPagar);
            this.tabPage1.Controls.Add(this.chkData);
            this.tabPage1.Controls.Add(this.chkFrequ);
            this.tabPage1.Controls.Add(this.chkCorte);
            this.tabPage1.Controls.Add(this.chkLapis);
            this.tabPage1.Controls.Add(this.chkBarba);
            this.tabPage1.Controls.Add(this.chkPezinho);
            this.tabPage1.Controls.Add(this.chkLuzes);
            this.tabPage1.Controls.Add(this.chkPigmentacaoBarba);
            this.tabPage1.Controls.Add(this.chkSombrancelha);
            this.tabPage1.Controls.Add(this.chkPigmentacaoCorte);
            this.tabPage1.Controls.Add(this.chkSombrancelhaHenna);
            this.tabPage1.Controls.Add(this.chkGel);
            this.tabPage1.Controls.Add(this.chkRelaxamento);
            this.tabPage1.Controls.Add(this.chkProgressiva);
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1174, 355);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dados do Corte";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(411, 305);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 25);
            this.label6.TabIndex = 10;
            this.label6.Text = "Valor R$";
            // 
            // txtValor
            // 
            this.txtValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValor.Location = new System.Drawing.Point(512, 302);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(109, 31);
            this.txtValor.TabIndex = 11;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "\"yyyy/MM/dd\"";
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(105, 322);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(100, 24);
            this.dateTimePicker1.TabIndex = 14;
            this.dateTimePicker1.Visible = false;
            // 
            // btnConcluir
            // 
            this.btnConcluir.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConcluir.Location = new System.Drawing.Point(814, 299);
            this.btnConcluir.Name = "btnConcluir";
            this.btnConcluir.Size = new System.Drawing.Size(113, 37);
            this.btnConcluir.TabIndex = 11;
            this.btnConcluir.Text = "Concluir";
            this.btnConcluir.UseVisualStyleBackColor = true;
            this.btnConcluir.Click += new System.EventHandler(this.btnConcluir_Click);
            // 
            // cboFreq
            // 
            this.cboFreq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFreq.Enabled = false;
            this.cboFreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFreq.FormattingEnabled = true;
            this.cboFreq.Items.AddRange(new object[] {
            "7 Dias",
            "10 Dias",
            "15 Dias",
            "20 Dias",
            "25 Dias",
            "30 Dias"});
            this.cboFreq.Location = new System.Drawing.Point(138, 272);
            this.cboFreq.Name = "cboFreq";
            this.cboFreq.Size = new System.Drawing.Size(121, 26);
            this.cboFreq.TabIndex = 17;
            // 
            // chkPagar
            // 
            this.chkPagar.AutoSize = true;
            this.chkPagar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPagar.Location = new System.Drawing.Point(665, 302);
            this.chkPagar.Name = "chkPagar";
            this.chkPagar.Size = new System.Drawing.Size(108, 29);
            this.chkPagar.TabIndex = 10;
            this.chkPagar.Text = "À Pagar";
            this.chkPagar.UseVisualStyleBackColor = true;
            // 
            // chkData
            // 
            this.chkData.AutoSize = true;
            this.chkData.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkData.Location = new System.Drawing.Point(25, 323);
            this.chkData.Name = "chkData";
            this.chkData.Size = new System.Drawing.Size(106, 22);
            this.chkData.TabIndex = 13;
            this.chkData.Text = "Ediar Data";
            this.chkData.UseVisualStyleBackColor = true;
            // 
            // chkFrequ
            // 
            this.chkFrequ.AutoSize = true;
            this.chkFrequ.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFrequ.Location = new System.Drawing.Point(25, 276);
            this.chkFrequ.Name = "chkFrequ";
            this.chkFrequ.Size = new System.Drawing.Size(100, 22);
            this.chkFrequ.TabIndex = 16;
            this.chkFrequ.Text = "Frequencia";
            this.chkFrequ.UseVisualStyleBackColor = true;
            // 
            // chkCorte
            // 
            this.chkCorte.AutoSize = true;
            this.chkCorte.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCorte.Location = new System.Drawing.Point(176, 34);
            this.chkCorte.Name = "chkCorte";
            this.chkCorte.Size = new System.Drawing.Size(83, 29);
            this.chkCorte.TabIndex = 0;
            this.chkCorte.Text = "Corte";
            this.chkCorte.UseVisualStyleBackColor = true;
            // 
            // chkLapis
            // 
            this.chkLapis.AutoSize = true;
            this.chkLapis.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLapis.Location = new System.Drawing.Point(176, 124);
            this.chkLapis.Name = "chkLapis";
            this.chkLapis.Size = new System.Drawing.Size(83, 29);
            this.chkLapis.TabIndex = 15;
            this.chkLapis.Text = "Lapis";
            this.chkLapis.UseVisualStyleBackColor = true;
            // 
            // chkBarba
            // 
            this.chkBarba.AutoSize = true;
            this.chkBarba.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBarba.Location = new System.Drawing.Point(176, 64);
            this.chkBarba.Name = "chkBarba";
            this.chkBarba.Size = new System.Drawing.Size(88, 29);
            this.chkBarba.TabIndex = 1;
            this.chkBarba.Text = "Barba";
            this.chkBarba.UseVisualStyleBackColor = true;
            // 
            // chkPezinho
            // 
            this.chkPezinho.AutoSize = true;
            this.chkPezinho.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPezinho.Location = new System.Drawing.Point(176, 184);
            this.chkPezinho.Name = "chkPezinho";
            this.chkPezinho.Size = new System.Drawing.Size(109, 29);
            this.chkPezinho.TabIndex = 2;
            this.chkPezinho.Text = "Pezinho";
            this.chkPezinho.UseVisualStyleBackColor = true;
            // 
            // chkLuzes
            // 
            this.chkLuzes.AutoSize = true;
            this.chkLuzes.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLuzes.Location = new System.Drawing.Point(176, 154);
            this.chkLuzes.Name = "chkLuzes";
            this.chkLuzes.Size = new System.Drawing.Size(89, 29);
            this.chkLuzes.TabIndex = 8;
            this.chkLuzes.Text = "Luzes";
            this.chkLuzes.UseVisualStyleBackColor = true;
            // 
            // chkPigmentacaoBarba
            // 
            this.chkPigmentacaoBarba.AutoSize = true;
            this.chkPigmentacaoBarba.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPigmentacaoBarba.Location = new System.Drawing.Point(330, 20);
            this.chkPigmentacaoBarba.Name = "chkPigmentacaoBarba";
            this.chkPigmentacaoBarba.Size = new System.Drawing.Size(219, 29);
            this.chkPigmentacaoBarba.TabIndex = 12;
            this.chkPigmentacaoBarba.Text = "Pigmentação Barba";
            this.chkPigmentacaoBarba.UseVisualStyleBackColor = true;
            // 
            // chkSombrancelha
            // 
            this.chkSombrancelha.AutoSize = true;
            this.chkSombrancelha.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSombrancelha.Location = new System.Drawing.Point(330, 164);
            this.chkSombrancelha.Name = "chkSombrancelha";
            this.chkSombrancelha.Size = new System.Drawing.Size(169, 29);
            this.chkSombrancelha.TabIndex = 5;
            this.chkSombrancelha.Text = "Sombrancelha";
            this.chkSombrancelha.UseVisualStyleBackColor = true;
            // 
            // chkPigmentacaoCorte
            // 
            this.chkPigmentacaoCorte.AutoSize = true;
            this.chkPigmentacaoCorte.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPigmentacaoCorte.Location = new System.Drawing.Point(330, 56);
            this.chkPigmentacaoCorte.Name = "chkPigmentacaoCorte";
            this.chkPigmentacaoCorte.Size = new System.Drawing.Size(214, 29);
            this.chkPigmentacaoCorte.TabIndex = 3;
            this.chkPigmentacaoCorte.Text = "Pigmentação Corte";
            this.chkPigmentacaoCorte.UseVisualStyleBackColor = true;
            // 
            // chkSombrancelhaHenna
            // 
            this.chkSombrancelhaHenna.AutoSize = true;
            this.chkSombrancelhaHenna.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSombrancelhaHenna.Location = new System.Drawing.Point(330, 200);
            this.chkSombrancelhaHenna.Name = "chkSombrancelhaHenna";
            this.chkSombrancelhaHenna.Size = new System.Drawing.Size(268, 29);
            this.chkSombrancelhaHenna.TabIndex = 4;
            this.chkSombrancelhaHenna.Text = "Sombrancelha de Henna";
            this.chkSombrancelhaHenna.UseVisualStyleBackColor = true;
            // 
            // chkGel
            // 
            this.chkGel.AutoSize = true;
            this.chkGel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGel.Location = new System.Drawing.Point(176, 94);
            this.chkGel.Name = "chkGel";
            this.chkGel.Size = new System.Drawing.Size(64, 29);
            this.chkGel.TabIndex = 9;
            this.chkGel.Text = "Gel";
            this.chkGel.UseVisualStyleBackColor = true;
            // 
            // chkRelaxamento
            // 
            this.chkRelaxamento.AutoSize = true;
            this.chkRelaxamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRelaxamento.Location = new System.Drawing.Point(330, 128);
            this.chkRelaxamento.Name = "chkRelaxamento";
            this.chkRelaxamento.Size = new System.Drawing.Size(157, 29);
            this.chkRelaxamento.TabIndex = 6;
            this.chkRelaxamento.Text = "Relaxamento";
            this.chkRelaxamento.UseVisualStyleBackColor = true;
            // 
            // chkProgressiva
            // 
            this.chkProgressiva.AutoSize = true;
            this.chkProgressiva.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkProgressiva.Location = new System.Drawing.Point(330, 92);
            this.chkProgressiva.Name = "chkProgressiva";
            this.chkProgressiva.Size = new System.Drawing.Size(145, 29);
            this.chkProgressiva.TabIndex = 7;
            this.chkProgressiva.Text = "Progressiva";
            this.chkProgressiva.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.chkInfantil);
            this.tabPage2.Controls.Add(this.lblInfantil);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.cboOperadora);
            this.tabPage2.Controls.Add(this.txtNomeCompleto);
            this.tabPage2.Controls.Add(this.chkMensalista);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.mskTelFixo);
            this.tabPage2.Controls.Add(this.txtEndereco);
            this.tabPage2.Controls.Add(this.mskTelCel);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.btnEditar);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.lblwhats);
            this.tabPage2.Controls.Add(this.lblMensalista);
            this.tabPage2.Controls.Add(this.chkWhats);
            this.tabPage2.Location = new System.Drawing.Point(4, 33);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1174, 355);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Dados do Cliente";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // chkInfantil
            // 
            this.chkInfantil.AutoSize = true;
            this.chkInfantil.Enabled = false;
            this.chkInfantil.Location = new System.Drawing.Point(256, 219);
            this.chkInfantil.Name = "chkInfantil";
            this.chkInfantil.Size = new System.Drawing.Size(15, 14);
            this.chkInfantil.TabIndex = 25;
            this.chkInfantil.UseVisualStyleBackColor = true;
            this.chkInfantil.Visible = false;
            // 
            // lblInfantil
            // 
            this.lblInfantil.AutoSize = true;
            this.lblInfantil.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfantil.Location = new System.Drawing.Point(277, 214);
            this.lblInfantil.Name = "lblInfantil";
            this.lblInfantil.Size = new System.Drawing.Size(75, 25);
            this.lblInfantil.TabIndex = 24;
            this.lblInfantil.Text = "Infantil";
            this.lblInfantil.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(365, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 25);
            this.label7.TabIndex = 10;
            this.label7.Text = "Nome";
            // 
            // cboOperadora
            // 
            this.cboOperadora.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOperadora.Enabled = false;
            this.cboOperadora.FormattingEnabled = true;
            this.cboOperadora.Items.AddRange(new object[] {
            "Tim",
            "Vivo",
            "Oi",
            "Claro",
            "Nextel"});
            this.cboOperadora.Location = new System.Drawing.Point(603, 124);
            this.cboOperadora.Name = "cboOperadora";
            this.cboOperadora.Size = new System.Drawing.Size(113, 32);
            this.cboOperadora.TabIndex = 23;
            // 
            // txtNomeCompleto
            // 
            this.txtNomeCompleto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomeCompleto.Location = new System.Drawing.Point(448, 39);
            this.txtNomeCompleto.Name = "txtNomeCompleto";
            this.txtNomeCompleto.ReadOnly = true;
            this.txtNomeCompleto.Size = new System.Drawing.Size(457, 31);
            this.txtNomeCompleto.TabIndex = 10;
            // 
            // chkMensalista
            // 
            this.chkMensalista.AutoSize = true;
            this.chkMensalista.Location = new System.Drawing.Point(372, 225);
            this.chkMensalista.Name = "chkMensalista";
            this.chkMensalista.Size = new System.Drawing.Size(15, 14);
            this.chkMensalista.TabIndex = 22;
            this.chkMensalista.UseVisualStyleBackColor = true;
            this.chkMensalista.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(338, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 25);
            this.label3.TabIndex = 12;
            this.label3.Text = "Endereco";
            // 
            // mskTelFixo
            // 
            this.mskTelFixo.Location = new System.Drawing.Point(450, 127);
            this.mskTelFixo.Mask = "(##)####-####";
            this.mskTelFixo.Name = "mskTelFixo";
            this.mskTelFixo.ReadOnly = true;
            this.mskTelFixo.Size = new System.Drawing.Size(147, 29);
            this.mskTelFixo.TabIndex = 21;
            // 
            // txtEndereco
            // 
            this.txtEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.Location = new System.Drawing.Point(448, 83);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.ReadOnly = true;
            this.txtEndereco.Size = new System.Drawing.Size(457, 31);
            this.txtEndereco.TabIndex = 11;
            // 
            // mskTelCel
            // 
            this.mskTelCel.Location = new System.Drawing.Point(450, 170);
            this.mskTelCel.Mask = "(##)#####-####";
            this.mskTelCel.Name = "mskTelCel";
            this.mskTelCel.ReadOnly = true;
            this.mskTelCel.Size = new System.Drawing.Size(147, 29);
            this.mskTelCel.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(301, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 25);
            this.label4.TabIndex = 14;
            this.label4.Text = "Telefone Fixo";
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditar.Location = new System.Drawing.Point(1013, 304);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(113, 36);
            this.btnEditar.TabIndex = 16;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(277, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(170, 25);
            this.label5.TabIndex = 16;
            this.label5.Text = "Telefone Celular";
            // 
            // lblwhats
            // 
            this.lblwhats.AutoSize = true;
            this.lblwhats.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblwhats.Location = new System.Drawing.Point(551, 219);
            this.lblwhats.Name = "lblwhats";
            this.lblwhats.Size = new System.Drawing.Size(111, 25);
            this.lblwhats.TabIndex = 19;
            this.lblwhats.Text = "WhatsApp";
            this.lblwhats.Visible = false;
            // 
            // lblMensalista
            // 
            this.lblMensalista.AutoSize = true;
            this.lblMensalista.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensalista.Location = new System.Drawing.Point(393, 218);
            this.lblMensalista.Name = "lblMensalista";
            this.lblMensalista.Size = new System.Drawing.Size(124, 25);
            this.lblMensalista.TabIndex = 17;
            this.lblMensalista.Text = "*Mensalista";
            this.lblMensalista.Visible = false;
            // 
            // chkWhats
            // 
            this.chkWhats.AutoSize = true;
            this.chkWhats.Enabled = false;
            this.chkWhats.Location = new System.Drawing.Point(535, 225);
            this.chkWhats.Name = "chkWhats";
            this.chkWhats.Size = new System.Drawing.Size(15, 14);
            this.chkWhats.TabIndex = 18;
            this.chkWhats.UseVisualStyleBackColor = true;
            this.chkWhats.Visible = false;
            // 
            // Consulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1251, 568);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtApelido);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Consulta";
            this.Text = "Consult";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtApelido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cboFreq;
        private System.Windows.Forms.CheckBox chkFrequ;
        private System.Windows.Forms.CheckBox chkLapis;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.CheckBox chkData;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Button btnConcluir;
        private System.Windows.Forms.CheckBox chkPagar;
        private System.Windows.Forms.CheckBox chkGel;
        private System.Windows.Forms.CheckBox chkProgressiva;
        private System.Windows.Forms.CheckBox chkRelaxamento;
        private System.Windows.Forms.CheckBox chkSombrancelhaHenna;
        private System.Windows.Forms.CheckBox chkCorte;
        private System.Windows.Forms.CheckBox chkBarba;
        private System.Windows.Forms.CheckBox chkPezinho;
        private System.Windows.Forms.CheckBox chkLuzes;
        private System.Windows.Forms.CheckBox chkPigmentacaoBarba;
        private System.Windows.Forms.CheckBox chkSombrancelha;
        private System.Windows.Forms.CheckBox chkPigmentacaoCorte;
        private System.Windows.Forms.CheckBox chkInfantil;
        private System.Windows.Forms.Label lblInfantil;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboOperadora;
        private System.Windows.Forms.TextBox txtNomeCompleto;
        private System.Windows.Forms.CheckBox chkMensalista;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox mskTelFixo;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.MaskedTextBox mskTelCel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblwhats;
        private System.Windows.Forms.Label lblMensalista;
        private System.Windows.Forms.CheckBox chkWhats;
    }
}